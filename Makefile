run-local:
	TT_ENV=local java -Dio.ktor.development=true -jar build/libs/turntable-all.jar

run-vm:
	REDIS_URL=localhost PORT=8000 java -jar build/libs/turntable-all.jar

test:
	./gradlew test

jar:
	./gradlew shadowJar

new-game:
	bin/new-game.sh
