
window.onload = function() {
    makeUid();
    populateGrid()
};

function populateGrid() {
    let request = new XMLHttpRequest();
    request.open('GET', 'gamelist');
    request.onreadystatechange = function() {
        if (request.readyState == XMLHttpRequest.DONE && request.status == 200) {
            JSON.parse(request.responseText).forEach(buildGridItem);
        }
    }
    request.send();
}

function buildGridItem(game) {

    var gameIFrame = document.createElement("iframe");
    gameIFrame.id = game;
    gameIFrame.className = "thumb";
    gameIFrame.src = game + "/thumb.html";
    gameIFrame.scrolling = "no";
    gameIFrame.addEventListener("load", function() {
        setThumbHeight();
        this.contentWindow.document.addEventListener("click", function() {
            popUpOptions(game);
        });
    });

    var gameDiv = document.createElement("div");
    gameDiv.className = "item";
    gameDiv.appendChild(gameIFrame);

    document.getElementById("grid").appendChild(gameDiv);

}

function popUpOptions(gameName) {

    var thumb = document.createElement("iframe");
    thumb.id = "popoup-" + gameName;
    thumb.className = "popup-thumb";
    thumb.src = gameName + "/thumb.html";
    thumb.scrolling = "no";

    thumb.addEventListener("load", function() {
        this.height = this.contentWindow.document.body.scrollHeight + 50;
    });

    var form = document.createElement("iframe");
    form.id = "form-" + gameName;
    form.className = "popup-form";
    form.src = gameName + "/form.html";
    form.scrolling = "no";

    form.addEventListener("load", function() {
        this.height = this.contentWindow.document.body.scrollHeight + 50;
    });

    var popup = document.getElementById("popupinner");
    popup.innerHTML="";
    popup.appendChild(thumb);
    popup.appendChild(form);
    document.getElementById("popup").style.visibility = "visible"
    window.scrollTo(0, 0);
}

function clearPopUp() {
    document.getElementById("popup").style.visibility = "hidden";
    document.getElementById("popupinner").innerHTML = "";
}

function setThumbHeight() {
    maxHeight = 150;
    let thumbs = document.getElementsByClassName("thumb");
    Array.from(thumbs).forEach((iframe) => {
        if ((iframe.contentDocument || iframe.contentWindow.document).readyState != "complete")
            return
        maxHeight = Math.max(maxHeight, iframe.contentWindow.document.body.scrollHeight);
    });
    maxHeight = maxHeight + 50;
    Array.from(thumbs).forEach((iframe) => {
        iframe.height = maxHeight;
    });
}
