
window.onload = function() {
    makeUid();
    startWebSocket();
}

var socket
var gameJson
var messageTime = Date.now()

function startWebSocket() {
    socket = new WebSocket(window.location.href.replace("http", "ws") + "/" + getUid());

    socket.onopen = function(event) {
        console.log("Connection established");
        socket.send("{}");
    }

    socket.onclose = function() {
        console.log("Lost Connection, Reconnecting...")
        socket = null
        setTimeout(startWebSocket, 5000)
    }

    socket.onmessage = function(event) {
        console.log(Date.now() - messageTime)
        let json = JSON.parse(event.data)
        if (json["errorMessage"] == null) {
            gameJson = json
            renderJson(json)
        } else {
            showErrorMessage(json["errorMessage"])
        }
    }
}

function showErrorMessage(message) {
    console.log(message)
}

function renderJson(json) {
    if (userIsNotAPlayer(json) && gameIsNotFull(json)) {
        renderLoginForm(document.getElementById("mainDiv"))
    }
    else if (gameIsNotFull(json)) {
        mainDiv = document.getElementById("mainDiv").innerHTML =
            "Waiting for other player...";
    }
    else {
        renderGame(json)
    }
}

function renderGame(json) {
    mainDiv = document.getElementById("mainDiv")
    mainDiv.innerHTML =
        "<h2>" + generateMessage(json) + '</h2> </br> </br> <button onClick=noYou()>No You</button>';
    if (!mainDiv.innerHTML.includes(otherPlayerName(json))) {
        document.body.style.backgroundColor = "red";
    } else {
        document.body.style.backgroundColor = "green";
    }

}

function noYou() {
    messageTime = Date.now()
    socket.send(JSON.stringify({
        "board" : otherPlayerName(gameJson)
    }))
}

function generateMessage(json) {
    if (json["board"] == null) {
        return json["players"]["1"]["name"] + " is " + json["msg"]
    } else {
        return json["board"] + " is " + json["msg"]
    }
}
