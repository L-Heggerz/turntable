
var socket = null
var gameJson = null

window.onload = function() {
    makeUid()
    startWebSocket()
    window.onresize = function() {
        if (document.activeElement === document.body) {
            renderJson(gameJson)
        }
    }
}

function startWebSocket() {
    socket = new WebSocket(window.location.href.replace("http", "ws") + "/" + getUid());

    socket.onopen = function(event) {
        console.log("Connection established");
        socket.send("{}");
    }

    socket.onclose = function() {
        console.log("Lost Connection, Reconnecting...")
        socket = null
        setTimeout(startWebSocket, 5000)
    }

    socket.onmessage = function(event) {
        let json = JSON.parse(event.data)
        if (json["errorMessage"] == null) {
            gameJson = json
            renderJson(json)
        } else {
            showErrorMessage(json["errorMessage"])
        }
    }
}

function showErrorMessage(message) {
    let errorDiv = document.getElementById("errorDiv")

    errorDiv.innerHTML = message
    errorDiv.style.visibility = "visible"

    setTimeout(function() {
        errorDiv.style.visibility = "hidden"
    }, 3000)
}

function renderJson(json) {
    if (userIsNotAPlayer(json) && gameIsNotFull(json)) {
        renderLoginForm(document.getElementById("mainDiv"))
    }
    else if (gameIsNotFull(json)) {
        mainDiv = document.getElementById("mainDiv").innerHTML = "Waiting for other player...";
    }
    else {
        renderGame(json)
    }
}

function renderGame(json) {
    window.onresize = function() {
        renderJson(gameJson)
    }

    height = window.innerHeight
    width = window.innerWidth

    if ( height < width ) {
        canvasSize = width / 2
        document.getElementById("mainDiv").innerHTML = `
            <canvas
                id="shots"
                width="${canvasSize}}"
                height="${canvasSize}"
                style="position: absolute;
                    left: 0%;
                    top: 50%;
                    -webkit-transform: translate(-0%, -50%);
                    transform: translate(-0%, -50%);"
            ></canvas>
            <canvas
                id="ships"
                width="${canvasSize}}"
                height="${canvasSize}"
                style="position: absolute;
                    left: 75%;
                    top: 50%;
                    -webkit-transform: translate(-100%, -50%);
                    transform: translate(-100%, -50%);"
            ></canvas>
        `
    } else {
        canvasSize = height / 2
        document.getElementById("mainDiv").innerHTML = `
            <canvas
                id="ships"
                width="${canvasSize}}"
                height="${canvasSize}"
                style="position: absolute;
                    left: 50%;
                    top: 0%;
                    -webkit-transform: translate(-50%, -0%);
                    transform: translate(-50%, -0%);"
            ></canvas>
            <canvas
                id="shots"
                width="${canvasSize}}"
                height="${canvasSize}"
                style="position: absolute;
                    left: 50%;
                    top: 100%;
                    -webkit-transform: translate(-50%, -100%);
                    transform: translate(-50%, -100%);"
            ></canvas>
        `
    }

    ships = document.getElementById("ships")
    shots = document.getElementById("shots")

    ship = ships.getContext("2d")
    shot = shots.getContext("2d")

    ship.fillStyle = "#5555EE";
    ship.fillRect(0, 0, ships.width, ships.height)

    shot.fillStyle = "#111111";
    shot.fillRect(0, 0, shots.width, shots.height)

    ship.stroke()
    shot.stroke()

    gridSize = parseInt(json["size"]) + 1

    drawLayout(ships, "#FFFFFF", gridSize)
    drawLayout(shots, "#11EE11", gridSize)

    myId = myIndex(json)
    theirId = myId == 1 ? 2 : 1

    drawBoats(ships, json["boats"][myId.toString()], "#999999", "#111111", gridSize)

    drawShots(ships, "#FFFFFF", "#EE4400", json["shots"][theirId.toString()], gridSize)
    drawShots(shots, "#11EE11", "#11EE11", json["shots"][myId.toString()], gridSize)

    if (everyoneIsReady(json)) {
        shots.onmouseup = function(event) {
            shoot(
                shots,
                json,
                event
            )
        }
    } else {
        drawGameSetup(json, shots, ships)

        placedShips = getShipCount(json)

        if (
            json["subs"] == placedShips["subs"] &&
            json["destroyers"] == placedShips["destroyers"] &&
            json["battleships"] == placedShips["battleships"] &&
            json["carriers"] == placedShips["carriers"] &&
            !json["readys"][myIndex(json)]
        ) {
            socket.send(JSON.stringify({ readys: { [myIndex(json)] : true } }))
        }
    }

    drawScreenEffect(shots)
}

var selectedShip = null
var selectedShipHorizontal = true
var selectedShipLocation = {
    x: null,
    y: null
}

function getShipCount(json) {
    var placedShips = {
        subs: 0,
        destroyers: 0,
        battleships: 0,
        carriers: 0
    }
    for ( keyX in json["boats"][myIndex(json)]) {
        for ( keyY in json["boats"][myIndex(json)][keyX] ) {
            var symbol = json["boats"][myIndex(json)][keyX][keyY]
            length = 0
            x = parseInt(keyX)
            y = parseInt(keyY)
            if (symbol == "<") {
                while (symbol != ">" && x + length < json["size"]) {
                    length ++
                    symbol = json["boats"][myIndex(json)][x + length][keyY]
                }
            }
            if (symbol == "^") {
                while (symbol != "v" && y + length < json["size"]) {
                    length ++
                    symbol = json["boats"][myIndex(json)][keyX][y + length]
                }
            }
            if ( length == 1 ) {
                placedShips.subs++
            }
            else if ( length == 2 ) {
                placedShips.destroyers++
            }
            else if ( length == 3 ){
                placedShips.battleships++
            } 
            else if ( length == 4 ) {
                placedShips.carriers++
            }
        }
    }
    return placedShips
}

function drawGameSetup(json, shots, ships) {
    drawBattleshipsLeftDisplay(json, shots)
    shots.onmousedown = function(event) {
        rect = shots.getBoundingClientRect()
        eventX = event.clientX - rect.left
        eventY = event.clientY - rect.top
        if (
            eventX > shots.width / 5 && eventX < 4 * shots.width / 5 &&
            eventY > shots.height / 5 && eventY < 4 * shots.width / 5
        ) {
            pickShip(json, eventY, shots.height)
        }
    }
    shots.onmousemove = function(event) {
        [eventX, eventY] = gridCoordinates(ships, event, json)
        selectedShipLocation = {
            x: eventX,
            y: eventY
        }
        drawGhostShip(
            selectedShip,
            selectedShipLocation,
            selectedShipHorizontal,
            JSON.parse(JSON.stringify(gameJson)),
            ships
        )
    }
    ships.onmousemove = shots.onmousemove
    document.onkeypress = function(event){
        if (event.code == "KeyR") {
            selectedShipHorizontal = !selectedShipHorizontal
        }
        drawGhostShip(
            selectedShip,
            selectedShipLocation,
            selectedShipHorizontal,
            JSON.parse(JSON.stringify(gameJson)),
            ships
        )
    }
    ships.onmouseup = function(event){
        if (selectedShip != null) {
            [eventX, eventY] = gridCoordinates(ships, event, gameJson)
            jsonUpdate = { boats : { [myIndex(gameJson)] : {} }}
            if (selectedShipHorizontal) {
                startX = Math.max(Math.min(eventX, gameJson["size"] - selectedShip + 1), 1)
                startY = Math.max(eventY, 1)
                jsonUpdate["boats"][myIndex(gameJson)][startX] = { [startY] : "<" }
                i = 1
                for (; i < selectedShip - 1; i++) {
                    jsonUpdate["boats"][myIndex(gameJson)][startX + i] = { [startY] : "-" }
                }
                jsonUpdate["boats"][myIndex(gameJson)][startX + i] = { [startY] : ">" }
            } else {
                startX = Math.max(eventX, 1)
                startY = Math.max(Math.min(eventY, gameJson["size"] - selectedShip + 1), 1)
                jsonUpdate["boats"][myIndex(gameJson)][startX] = { [startY] : "^" }
                i = 1
                for (; i < selectedShip - 1; i++) {
                    jsonUpdate["boats"][myIndex(gameJson)][startX][startY + i] = "|"
                }
                jsonUpdate["boats"][myIndex(gameJson)][startX][startY + i] = "v"
            }
            socket.send(JSON.stringify(jsonUpdate))
        }
        selectedShip = null
    }
}

function drawGhostShip(type, location, horizontal, jsonCopy, canvas) {
    if (type != null) {
        if (horizontal) {
            startX = Math.max(Math.min(location.x, jsonCopy["size"] - type + 1), 1)
            startY = Math.max(location.y, 1)
            jsonCopy["boats"][myIndex(jsonCopy)][startX][startY] = "<"
            i = 1
            for (; i < type - 1; i++) {
                jsonCopy["boats"][myIndex(jsonCopy)][startX + i][startY] = "-"
            }
            jsonCopy["boats"][myIndex(jsonCopy)][startX + i][startY] = ">"
        } else {
            startX = Math.max(location.x, 1)
            startY = Math.max(Math.min(location.y, jsonCopy["size"] - type + 1), 1)
            jsonCopy["boats"][myIndex(jsonCopy)][startX][startY] = "^"
            i = 1
            for (; i < type - 1; i++) {
                jsonCopy["boats"][myIndex(jsonCopy)][startX][startY + i] = "|"
            }
            jsonCopy["boats"][myIndex(jsonCopy)][startX][startY + i] = "v"
        }
        ship = canvas.getContext("2d")
    
        ship.fillStyle = "#5555EE";
        ship.fillRect(0, 0, ships.width, ships.height)
    
        ship.stroke()
        shot.stroke()
    
        gridSize = parseInt(gameJson["size"]) + 1
    
        drawLayout(ships, "#FFFFFF", gridSize)

        drawBoats(ships, gameJson["boats"][myIndex(gameJson)], "#999999", "#111111", gridSize)
    
        drawBoats(canvas, jsonCopy["boats"][myIndex(jsonCopy)], "#999999", "#111111", gridSize)
    }
}

function pickShip(json, y, height) {
    placedShips = getShipCount(json)
    if (y < height * (30/80) && json["subs"] - placedShips["subs"] > 0) {
        selectedShip = 2
    } else if ( y < height * (39/80) && json["destroyers"] - placedShips["destroyers"] > 0) {
        selectedShip = 3
    } else if ( y < height * (48/80) && json["battleships"] - placedShips["battleships"] > 0) {
        selectedShip = 4
    } else if ( y < height * (57/80) && json["carriers"] - placedShips["carriers"] > 0) {
        selectedShip = 5
    } else {
        selectedShipHorizontal = !selectedShipHorizontal
    }
}

function drawBattleshipsLeftDisplay(json, shots) {
    p = shots.getContext("2d")
    p.save()
    p.beginPath()
    p.lineWidth = 5
    p.strokeStyle = "#11EE11"
    p.fillStyle - "#111111"
    p.rect(shots.height / 5, shots.width / 5, 3 * shots.height / 5, 3 * shots.width / 5)
    p.stroke()
    p.fill()

    p.fillStyle = "#11EE11"
    p.textBaseline = 'top'
    p.textAlign = 'center'
    p.font = `bold ${shots.height / 25}px Arial`
    p.fillText("Place Your Ships", shots.width / 2, 1.05 * shots.height / 5)

    placedShips = getShipCount(json)

    if (
        json["subs"] == placedShips["subs"] &&
        json["destroyers"] == placedShips["destroyers"] &&
        json["battleships"] == placedShips["battleships"] &&
        json["carriers"] == placedShips["carriers"]
    ) {
        p.fillText("Ready", shots.width / 2, 3.55 * shots.height / 5)
    } else {
        p.fillText("\u{1F141}otate", shots.width / 2, 3.55 * shots.height / 5)
    }

    p.textAlign = 'right'
    p.fillText(`subs x ${json["subs"] - placedShips["subs"]}`, 19 * shots.width / 24, 3 * shots.height / 10)
    p.fillText(`destroyers x ${json["destroyers"] - placedShips["destroyers"]}`, 19 * shots.width / 24, 4 * shots.height / 10)
    p.fillText(`battleships x ${json["battleships"] - placedShips["battleships"]}`, 19 * shots.width / 24, 5 * shots.height / 10)
    p.fillText(`carriers x ${json["carriers"] - placedShips["carriers"]}`, 19 * shots.width / 24, 6 * shots.height / 10)



    p.beginPath()
    p.lineWidth = 1
    p.fillStyle = "#5555EE"
    p.strokeStyle = "#11EE11"

    p.rect(6 * shots.width / 18, 3 * shots.height / 10, shots.width / 9, shots.width / 18)
    p.rect(11 * shots.width / 36, 4 * shots.height / 10, 3 * shots.width / 18, shots.width / 18)
    p.rect(10 * shots.width / 36, 5 * shots.height / 10, 4 * shots.width / 18, shots.width / 18)
    p.rect(9 * shots.width / 36, 6 * shots.height / 10, 5 * shots.width / 18, shots.width / 18)

    p.fill()
    p.stroke()

    p.fillStyle = "#999999"
    p.strokeStyle = "#111111"


    p.beginPath()
    p.ellipse(
        14 * shots.width / 36,
        118 * shots.height / 360,
        shots.width / 24,
        shots.height / 48,
        0,
        2 * Math.PI,
        0,
        false
    )
    p.closePath()
    p.fill()
    p.stroke()

    p.beginPath()
    p.ellipse(
        14 * shots.width / 36,
        154 * shots.height / 360,
        shots.width / 14,
        shots.height / 48,
        0,
        2 * Math.PI,
        0,
        false
    )
    p.closePath()
    p.fill()
    p.stroke()

    p.beginPath()
    p.ellipse(
        14 * shots.width / 36,
        190 * shots.height / 360,
        shots.width / 10,
        shots.height / 48,
        0,
        2 * Math.PI,
        0,
        false
    )
    p.closePath()
    p.fill()
    p.stroke()

    p.beginPath()
    p.ellipse(
        14 * shots.width / 36,
        226 * shots.height / 360,
        shots.width / 8,
        shots.height / 48,
        0,
        2 * Math.PI,
        0,
        false
    )
    p.closePath()
    p.fill()
    p.stroke()

    p.restore()
}

function drawMapEffect(canvas) {
    p = canvas.getContext("2d")
    p.save()
    p.beginPath()
    p.lineWidth = 1
    p.strokeStyle = "#000000"
    p.globalAlpha = 0.4;

    for ( i = 0; i < Math.pow(canvas.height, 2) / 200; i++ ) {
        x = random(0, canvas.width)
        y = random(0, canvas.height)
        p.moveTo(x,y)
        p.lineTo(x + 1,y)
    }
    p.stroke()
    p.restore()
}

function drawScreenEffect(canvas) {
    p = canvas.getContext("2d")
    p.save()
    p.lineWidth = 1
    p.strokeStyle = "#111111"
    p.beginPath()

    for ( i = 0; i < canvas.height; i += 3) {
        p.moveTo(0, i)
        p.lineTo(canvas.width, i)
    }
    p.stroke()
    p.restore()
}

function random(floor, ceiling) {
    return floor + Math.floor(Math.random() * (ceiling - floor))
}

function shoot(canvas, json, event) {
    [x, y] = gridCoordinates(canvas, event, json)
    socket.send(JSON.stringify(
        {
            shots : {
                [myIndex(json).toString()] : {
                    [x.toString()] : {
                        [y.toString()] : "x"
                    }
                }
            }
        }
    ))
}

function drawBoats(canvas, json, fillColour, strokeColour, gridSize) {
    p = canvas.getContext("2d")
    p.save()
    p.fillStyle = fillColour
    p.strokeStyle = strokeColour
    p.lineWidth = 1
    gridWidth = canvas.height / gridSize 

    for ( keyX in json) {
        x = parseInt(keyX)
        for ( keyY in json[keyX] ) {
            y = parseInt(keyY)
            if ( json[keyX][keyY] == "|" ) {
                p.rect(
                    ( x + 0.2 ) * gridWidth, 
                    y * gridWidth,
                    0.6 * gridWidth, 
                    gridWidth
                )
            } else if ( json[keyX][keyY] == "-" ) {
                p.rect(
                    x * gridWidth, 
                    ( y + 0.2 ) * gridWidth,
                    gridWidth, 
                    0.6 * gridWidth
                )

            } else if ( json[keyX][keyY] == "^" ) {
                p.beginPath()
                p.ellipse(
                    (x + 0.5) * gridWidth, 
                    ( y + 1 ) * gridWidth,
                    0.3 * gridWidth, 
                    0.8 * gridWidth,
                    0,
                    0,
                    Math.PI,
                    true
                )
                p.closePath()
            } else if ( json[keyX][keyY] == "v" ) {
                p.beginPath()
                p.ellipse(
                    (x + 0.5) * gridWidth, 
                    ( y ) * gridWidth,
                    0.3 * gridWidth, 
                    0.8 * gridWidth,
                    0,
                    0,
                    Math.PI,
                    false
                )
                p.closePath()
            } else if ( json[keyX][keyY] == "<" ) {
                p.beginPath()
                p.ellipse(
                    ( x + 1 ) * gridWidth, 
                    ( y + 0.5 ) * gridWidth,
                    0.8 * gridWidth, 
                    0.3 * gridWidth,
                    0,
                    Math.PI * 0.5,
                    Math.PI * 1.5,
                    false
                )
                p.closePath()
            } else if ( json[keyX][keyY] == ">" ) {
                p.beginPath()
                p.ellipse(
                    x * gridWidth, 
                    ( y + 0.5 ) * gridWidth,
                    0.8 * gridWidth, 
                    0.3 * gridWidth,
                    0,
                    Math.PI * 0.5,
                    Math.PI * 1.5,
                    true
                )
                p.closePath()
            }
            p.fill()
            p.stroke()
        }
    }
    p.restore()
}

function drawShots(canvas, missColour, hitColour, shots, gridSize) {
    p = canvas.getContext("2d")
    p.save()
    p.beginPath()
    p.textBaseline = 'middle'
    p.textAlign = 'center'
    gridWidth = canvas.height / gridSize
    p.font = `bold ${gridWidth}px Arial`

    x = 0
    for ( keyX in shots) {
        x++
        y = 0
        for ( keyY in shots[keyX] ) {
            y++
            if ( shots[keyX][keyY] == "MISS" ) {
                p.fillStyle = missColour
                p.fillText("\u{00D7}", ( ( x + 0.5 ) * gridWidth), ( ( y + 0.55 ) * gridWidth))
            } else if ( shots[keyX][keyY] == "HIT" ) {
                p.fillStyle = hitColour
                p.fillText("\u{2739}", ( ( x + 0.5 ) * gridWidth), ( ( y + 0.55 ) * gridWidth))
                p.beginPath()
                p.strokeStyle = missColour
                p.lineWidth = 0.25
                p.strokeText("\u{2739}", ( ( x + 0.5 ) * gridWidth), ( ( y + 0.55 ) * gridWidth))

            }
        }
    }
    p.stroke()
    p.restore()
}

function drawLayout(canvas, colour, size) {
    drawGrid(
        canvas,
        colour,
        size
    )

    drawVerticalNumbers(
        canvas,
        colour,
        size
    )

    drawHorizontalLetters(
        canvas,
        colour,
        size
    )
}

function drawVerticalNumbers(canvas, colour, size) {
    gridHeight = canvas.height / size

    p = canvas.getContext("2d")
    p.save()
    p.beginPath()
    p.font = `bold ${gridHeight}px Courier New`
    p.textBaseline = 'middle'
    p.textAlign = 'center'
    p.fillStyle = colour
    for (k = 1; k < gridSize; k++) {
        p.fillText(k, gridHeight / 2, ((gridHeight / 2) * 1.1) + (k * gridWidth))
    }
    p.stroke()
    p.restore()
}

function drawHorizontalLetters(canvas, colour, size) {
    const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    gridWidth = canvas.width / size

    p = canvas.getContext("2d")
    p.save()
    p.beginPath()
    p.font = `bold ${gridWidth}px Courier New`
    p.textBaseline = 'middle'
    p.textAlign = 'center'
    p.fillStyle = colour
    for (k = 1; k < gridSize; k++) {
        p.fillText(alphabet[k-1], (gridWidth / 2) + (k * gridWidth), (gridWidth / 2) * 1.1)
    }
    p.stroke()
    p.restore()
}

function drawGrid(canvas, colour, size) {
    p = canvas.getContext("2d")
    p.save()
    p.beginPath()
    p.strokeStyle = colour
    p.lineWidth = 2

    gridWidth = canvas.width / size
    for (i = 1; i <= size; i++) {
        p.moveTo(i * gridWidth, 0)
        p.lineTo(i * gridWidth, canvas.width)
    }

    gridHeight = canvas.height / size
    for (j = 1; j <= size; j++) {
        p.moveTo(0, j * gridHeight)
        p.lineTo(canvas.height, j * gridHeight)
    }
    
    p.stroke()
    p.restore()
}
