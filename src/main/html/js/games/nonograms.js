
var socket = null

window.onload = function() {
    makeUid()
    startWebSocket()
    window.onresize = function() {
        if (document.activeElement === document.body) {
            renderJson(gameJson)
        }
    }
}

function startWebSocket() {
    socket = new WebSocket(window.location.href.replace("http", "ws") + "/" + getUid());

    socket.onopen = function(event) {
        console.log("Connection established");
        socket.send("{}");
    }

    socket.onclose = function() {
        console.log("Lost Connection, Reconnecting...")
        socket = null
        setTimeout(startWebSocket, 5000)
    }

    socket.onmessage = function(event) {
        let json = JSON.parse(event.data)
        if (json["errorMessage"] == null) {
            gameJson = json
            renderJson(json)
        } else {
            showErrorMessage(json["errorMessage"])
        }
    }
}

function showErrorMessage(message) {
    let errorDiv = document.getElementById("errorDiv")

    errorDiv.innerHTML = message
    errorDiv.style.visibility = "visible"

    setTimeout(function() {
        errorDiv.style.visibility = "hidden"
    }, 3000)
}

function renderJson(json) {

    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)

    size = Math.min(vw, vh)
    document.getElementById("mainDiv").innerHTML = `
        <canvas id="board" width="${size}" height="${size}"></canvas>
    `

    canvas = document.getElementById("board")

    addEventListeners(canvas)

    drawNonagram(canvas, json)

    renderNewNonogramDiv(json)
}

function addEventListeners(canvas) {
    canvas.oncontextmenu = function (event) {
        event.preventDefault()
    };

    canvas.onmouseup = function(event) {

        [gridX, gridY] = gridCoordinates(canvas, event, gameJson)

        if (gridX <= 0 || gridY <= 0) {
            return
        }

        if ( event.button == 0 ) {
            sendCharacter(".", gridX, gridY, gameJson)
        } else
        if ( event.button == 2 ) {
            sendCharacter("X", gridX, gridY, gameJson, canvas)
        }

    }

    canvas.ontouchstart = function(event) {
        event.preventDefault()

        xy = gridCoordinates(canvas, event)

        gridX = xy[0]
        gridY = xy[1]

        if (gridX < 0 || gridY < 0) {
            return
        }

        currentChar = gameJson["grid"][gridY][gridX]

        if ( currentChar == "" ) {
            sendCharacter(".", gridX, gridY, gameJson)
        } else
        if ( currentChar == "." ) {
            sendCharacter("X", gridX, gridY, gameJson, canvas)
        } else {
            sendCharacter("", gridX, gridY, gameJson, canvas)
        }

    }
}

function sendCharacter(character, x, y, json) {
    charToSend = character
    if ( json["grid"][y.toString()][x.toString()] == character ) {
        charToSend = ""
    }
    socket.send(JSON.stringify(
        {
            "grid" : {
                [y.toString()] : {
                    [x.toString()] : charToSend
                }
            }
        }
    ))
}

function drawNonagram(canvas, json) {

    p = canvas.getContext("2d")

    drawGrid(p, json)

    drawClues(p, json)

    drawMarks(p, json)

    p.stroke()

}

function drawGrid(p, json) {
    size = canvas.height
    gridCount = calculateGridSize(json)
    gridSize = size / gridCount

    k = 0

    for ( i = gridCount - json["height"]; i <= gridCount; i++ ) {
        p.moveTo( 0, (  i * gridSize ) - 1 )
        p.lineTo( size, ( i * gridSize ) - 1 )
        if ( k % 5 == 0 ) {
            p.moveTo( 0, (  i * gridSize ) )
            p.lineTo( size, ( i * gridSize ) )
            p.moveTo( 0, (  i * gridSize ) - 2 )
            p.lineTo( size, ( i * gridSize ) - 2 )
        }
        k++
    }

    k = 0

    for ( j = gridCount - json["width"]; j <= gridCount; j++ ) {
        p.moveTo( ( j * gridSize ) - 1, 0 )
        p.lineTo( ( j * gridSize ) - 1, size )
        if ( k % 5 == 0 ) {
            p.moveTo( ( j * gridSize ), 0 )
            p.lineTo( ( j * gridSize ), size )
            p.moveTo( ( j * gridSize ) - 2, 0 )
            p.lineTo( ( j * gridSize ) - 2, size )
        }
        k++
    }
}

function drawClues(p, json) {
    size = canvas.height
    gridCount = calculateGridSize(json)
    gridSize = size / gridCount

    p.font = `bold ${gridSize}px Arial`
    p.textBaseline = 'top';
    p.textAlign = 'right';

    for ( k = gridCount - gameJson["height"]; k <= gridCount; k++ ) {
        clueIndex = k - ( gridCount - gameJson["height"] )

        clues = ""
        for (key in json["leftClues"][clueIndex.toString()]) {
            clues = clues + " " + json["leftClues"][clueIndex.toString()][key]
        }

        p.fillText(
            clues + " ",
            (gridCount - json["width"]) * gridSize,
            k * gridSize
        )
    }

    p.textBaseline = 'bottom';
    p.textAlign = 'center';

    for ( l = gridCount - json["width"]; l <= gridCount; l++ ) {
        clueIndex = l - ( gridCount - json["width"] )
        m = 0

        for (key in json["topClues"][clueIndex.toString()]) {
            p.fillText(
                json["topClues"][clueIndex.toString()][key],
                (l + 0.5) * gridSize,
                ( ( gridCount - json["height"] ) * gridSize ) - ( json["topClues"][clueIndex.toString()].length * gridSize ) + ( ( m + 1 ) * gridSize )
            )
            m++
        }
    }
}

function drawMarks(p, json) {
    size = canvas.height
    gridCount = calculateGridSize(json)
    gridSize = size / gridCount

    p.textBaseline = 'middle';
    p.textAlign = 'center';

    y = gridCount - gameJson["height"]

    for ( keyX in json["grid"] ) {
        x = gridCount - json["width"]
        for ( keyY in json["grid"][keyX] ) {
            mark = json["grid"][keyX][keyY]
            if ( mark == "X" ) {
                p.fillStyle = "red"
                p.fillText("\u{00D7}", (x + 0.5) * gridSize, (y + 0.55) * gridSize)
                p.fillStyle = "black"
            } else
            if ( mark == "." ) {
                p.fillRect((x * gridSize) - 1, (y * gridSize) - 1, gridSize, gridSize)
            }
            x++
        }
        y++
    }
}

var newNonogramDivVisibility = true

function renderNewNonogramDiv(json) {

    if (document.getElementById("reset") == null) {
        var elem = document.createElement('div');
        elem.id = "reset"
        document.body.appendChild(elem);
    }

    if (newNonogramDivVisibility) {
        document.getElementById("reset").innerHTML =
            '<label for="width" id="width-label">Width:</label>' +
            '<input type="number" id="width" name="width" style="width: 2em" value="' + json["width"] + '" min="4" max="30">  ' +
            '<label for="height" id="height-label">Height:</label>' +
            '<input type="number" id="height" name="height" style="width: 2em" value="' + json ["height"] + '" min="4" max="30">  ' +
            '<button onclick="requestNewNonogram()" style="padding: 10px 10px; font-size: 75%;">Reset</button>' +
            '<button onclick="toggleNonogramDiv()" style="padding: 10px 10px; border: none; background-color: white; color: grey;">&#8249;</button>'
    } else {
        document.getElementById("reset").innerHTML =
            '<button onclick="toggleNonogramDiv()" style="padding: 10px 10px; border: none; background-color: white; color: grey;">&#8250;</button>'
    }
}

function toggleNonogramDiv() {
    newNonogramDivVisibility = !newNonogramDivVisibility
    renderNewNonogramDiv(gameJson)
}

function requestNewNonogram() {
    socket.send(
        JSON.stringify(
            {
                newNonogram : true,
                width : document.getElementById("width").value,
                height : document.getElementById("height").value
            }
        )
    )
}
