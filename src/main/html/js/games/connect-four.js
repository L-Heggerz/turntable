
var socket = null

window.onload = function() {
    makeUid()
    startWebSocket()
    window.onresize = function() {
        renderJson(gameJson)
    }
}

function startWebSocket() {
    socket = new WebSocket(window.location.href.replace("http", "ws") + "/" + getUid());

    socket.onopen = function(event) {
        console.log("Connection established");
        socket.send("{}");
    }

    socket.onclose = function() {
        console.log("Lost Connection, Reconnecting...")
        socket = null
        setTimeout(startWebSocket, 5000)
    }

    socket.onmessage = function(event) {
        console.log("rendering")
        let json = JSON.parse(event.data)
        if (json["errorMessage"] == null) {
            gameJson = json
            renderJson(json)
        } else {
            showErrorMessage(json["errorMessage"])
        }
    }
}

function showErrorMessage(message) {
    let errorDiv = document.getElementById("errorDiv")

    errorDiv.innerHTML = message
    errorDiv.style.visibility = "visible"

    setTimeout(function() {
        errorDiv.style.visibility = "hidden"
    }, 3000)
}

function renderJson(json) {
    if (userIsNotAPlayer(json) && gameIsNotFull(json)) {
        renderLoginForm(document.getElementById("mainDiv"))
    }
    else if (gameIsNotFull(json)) {
        mainDiv = document.getElementById("mainDiv").innerHTML =
            "Waiting for other player...";
    }
    else {
        renderGame(json)
        renderResetDiv(json)
        renderColourDiv(json)
    }
}

function renderGame(json) {
    window.onresize = function() {
        if (document.activeElement === document.body) {
            renderJson(gameJson)
        }
    }

    height = window.innerHeight
    width = window.innerWidth

    if (width <= (height * (json["width"] / json["height"]))) {
        height = width * (json["height"] / json["width"])
    }
    else {
        width = height * (json["width"] / json["height"])
    }

    document.getElementById("mainDiv").innerHTML = `
        <canvas id="board" width="${width}" height="${height}"></canvas>

    `
    canvas = document.getElementById("board")
    canvas.onmouseup = function(event) {
        const rect = canvas.getBoundingClientRect()
        const x = event.clientX - rect.left
        sendMove(x, canvas.width)
    }

    drawFrame(json, canvas)
    drawPieces(json, canvas)

    potentialWinner = findWinner(json)
    if ( potentialWinner != null ) {
        drawWin(json["players"][parseInt(potentialWinner)]["name"], canvas, json)
    }
}

function findWinner(json) {
    board = json["board"]

    for ( col in board ) {
        if ( board[col].toString().replaceAll(",", "").includes("1111") ) {
            return "1"
        }
        if ( board[col].toString().replaceAll(",", "").includes("2222") ) {
            return "2"
        }
    }

    for ( x = 0; x < (json["width"] - 1); x++ ) {
        for ( y = 0; y < (json["height"] - 1); y++ ) {
            if (
                board[x.toString()][y] != null &&
                board[x.toString()][y] == board[(x + 1).toString()][y] &&
                board[x.toString()][y] == board[(x + 2).toString()][y] &&
                board[x.toString()][y] == board[(x + 3).toString()][y]
            ) {
                return board[x.toString()][y.toString()]
            }
        }
    }

    for ( x = 0; x < (json["width"] - 1); x++ ) {
        for ( y = 0; y < (json["height"] - 2); y++ ) {
            if (
                board[x.toString()][y] != null &&
                board[x.toString()][y] == board[(x + 1).toString()][(y + 1)] &&
                board[x.toString()][y] == board[(x + 2).toString()][(y + 2)] &&
                board[x.toString()][y] == board[(x + 3).toString()][(y + 3)]
            ) {
                return board[x.toString()][y.toString()]
            }
        }
    }

    for ( x = 0; x < (json["width"] - 1); x++ ) {
        for ( y = 3; y < json["height"]; y++ ) {
            if (
                board[x.toString()][y] != null &&
                board[x.toString()][y] == board[(x + 1).toString()][(y - 1)] &&
                board[x.toString()][y] == board[(x + 2).toString()][(y - 2)] &&
                board[x.toString()][y] == board[(x + 3).toString()][(y - 3)]
            ) {
                return board[x.toString()][y.toString()]
            }
        }
    }

    return null
}

function drawWin(name, canvas, json) {
    canvas.onmouseup = null
    f = canvas.getContext("2d")
    f.fillStyle = "rgba(255, 255, 255, 0.5)"
    f.rect(0, 0, canvas.width, canvas.height)
    f.fill()


    f.font = `bold ${canvas.height/10}px Arial`
    f.textBaseline = 'middle';
    f.textAlign = 'center';

    f.fillStyle = getColour(playerIndex(name, json), json)

    f.fillText(name + " Wins!", canvas.width / 2, canvas.height / 2)
}

function sendMove(x, width) {
    if ( x > 0 && x < width ) {
        columnWidth = width / gameJson["width"]
        column = Math.floor(x / columnWidth)
        socket.send(JSON.stringify(
            {
                board : {
                    [column] : myIndex(gameJson)
                }
            }
        ))
    }
}

function drawPieces(json, canvas) {
    gridSize = canvas.width / json["width"]
    for ( col in json["board"] ) {
        for ( piece in json["board"][col] ) {

            x = parseInt(col)
            y = (json["height"] - 1) - parseInt(piece)

            context = canvas.getContext("2d");
            context.beginPath();
            context.arc(
                (x * gridSize) + gridSize * 0.5,
                (y * gridSize) + gridSize * 0.5,
                gridSize * 0.39,
                0,
                2 * Math.PI,
                false
            );
            context.strokeStyle = 'transparent';

            if ( json["board"][col][piece] == "1" ) {
                context.fillStyle = getColour(1, json)
            }
            else if ( json["board"][col][piece] == "2" ) {
                context.fillStyle = getColour(2, json)
            }

            context.fill();
            context.stroke();
        }
    }
}

function drawFrame(json, canvas) {
    var context = canvas.getContext("2d");
    var gradient = context.createLinearGradient(0, 0, width, height);
    gradient.addColorStop(0, "#0000FF");
    gradient.addColorStop(0.5, "#0000AA");
    gradient.addColorStop(1, "#0000FF");
    context.fillStyle = gradient;
    context.fillRect(0, 0, canvas.width, canvas.height);

    gridSize = canvas.height / json["height"]

    for ( x = 0; x < json["width"]; x++ ) {
        for ( y = 0; y < json["height"]; y++ ) {
            context.beginPath();
            context.arc(
                (x * gridSize) + gridSize * 0.5,
                (y * gridSize) + gridSize * 0.5,
                gridSize * 0.4,
                0,
                2 * Math.PI,
                false
            );
            context.fillStyle = 'DarkBlue';
            context.fill();
            context.lineWidth = gridSize * 0.02;
            context.strokeStyle = 'DodgerBlue';
            context.setLineDash([3, 2]);
            context.stroke();
        }
    }

}

