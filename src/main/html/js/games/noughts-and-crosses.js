
var socket = null
var gameJson = null

window.onload = function() {
    makeUid()
    startWebSocket()
}

function startWebSocket() {
    socket = new WebSocket(window.location.href.replace("http", "ws") + "/" + getUid());

    socket.onopen = function(event) {
        console.log("Connection established");
        socket.send("{}");
    }

    socket.onclose = function() {
        console.log("Lost Connection, Reconnecting...")
        socket = null
        setTimeout(startWebSocket, 5000)
    }

    socket.onmessage = function(event) {
        let json = JSON.parse(event.data)
        if (json["errorMessage"] == null) {
            gameJson = json
            renderJson(json)
        } else {
            showErrorMessage(json["errorMessage"])
        }
    }
}

function showErrorMessage(message) {
    let errorDiv = document.getElementById("errorDiv")

    errorDiv.innerHTML = message
    errorDiv.style.visibility = "visible"

    setTimeout(function() {
        errorDiv.style.visibility = "hidden"
    }, 3000)
}

function renderJson(json) {
    if (userIsNotAPlayer(json) && gameIsNotFull(json)) {
        renderLoginForm(document.getElementById("mainDiv"))
    }
    else if (gameIsNotFull(json)) {
        mainDiv = document.getElementById("mainDiv").innerHTML =
            "Waiting for other player...";
    }
    else {
        renderGame(json)
        renderResetDiv(json)
        renderColourDiv(json)
    }
}

function renderGame(json) {
    window.onresize = function() {
        if (document.activeElement === document.body) {
            renderJson(gameJson)
        }
    }

    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)

    size = Math.min(vw, vh)
    document.getElementById("mainDiv").innerHTML = `
        <canvas id="board" width="${size}" height="${size}"></canvas>

    `
    canvas = document.getElementById("board")
    canvas.onmouseup = function(event) {
        const rect = canvas.getBoundingClientRect()
        const x = event.clientX - rect.left
        const y = event.clientY - rect.top
        sendMove(x, y, canvas.height/3)
    }

    drawBoard(canvas)
    drawMarks(canvas, json)

    if (winConditionsMet(json)) {
        drawWin(canvas, json)
    } else
    if (Object.values(gameJson["board"]).filter(it => it == "").length == 0) {
        drawTie(canvas)
    }

}

function winConditionsMet(json) {
    board = json["board"]
    lines = []


    lines.push(board["topLeft"] + board["topMid"] + board["topRight"])
    lines.push(board["midLeft"] + board["mid"] + board["midRight"])
    lines.push(board["bottomLeft"] + board["bottomMid"] + board["bottomRight"])

    lines.push(board["topLeft"] + board["midLeft"] + board["bottomLeft"])
    lines.push(board["topMid"] + board["mid"] + board["bottomMid"])
    lines.push(board["topRight"] + board["midRight"] + board["bottomRight"])

    lines.push(board["topLeft"] + board["mid"] + board["bottomRight"])
    lines.push(board["bottomLeft"] + board["mid"] + board["topRight"])

    return (lines.filter(line => line == "XXX" || line == "OOO" ).length > 0)
}

function drawWin(canvas, json) {
    canvas.onmouseup = null
    size = canvas.height
    f = canvas.getContext("2d")
    f.fillStyle = "rgba(255, 255, 255, 0.5)"
    f.rect(0, 0, size, size)
    f.fill()


    f.font = `bold ${size/10}px Arial`
    f.textBaseline = 'middle';
    f.textAlign = 'center';

    winner = playerNames(json).filter(it => it != currentPlayer(json))[0]

    f.fillStyle = getColour(playerIndex(winner, json), json)

    f.fillText(winner + " Wins!", size / 2, size / 2)
}

function drawTie(canvas) {
    canvas.onmouseup = null
    size = canvas.height
    f = canvas.getContext("2d")
    f.fillStyle = "rgba(255, 255, 255, 0.5)"
    f.rect(0, 0, size, size)
    f.fill()


    f.font = `bold ${size/10}px Arial`
    f.textBaseline = 'middle';
    f.textAlign = 'center';

    f.fillStyle = "rgba(0, 0, 0)"

    f.fillText("Draw", size / 2, size / 2)
}

function drawBoard(canvas) {
    size = canvas.height

    p = canvas.getContext("2d")

    p.moveTo(size / 3, 0)
    p.lineTo(size / 3, size)

    p.moveTo((2 * size) / 3, 0)
    p.lineTo((2 * size) / 3, size)

    p.moveTo(0, size / 3)
    p.lineTo(size, size / 3)

    p.moveTo(0, (2 * size) / 3)
    p.lineTo(size, (2 * size) / 3)

    p.stroke()
}

function drawMarks(canvas, json) {
    size = canvas.height / 3

    f = canvas.getContext("2d")
    f.font = `bold ${size}px Arial`
    f.textBaseline = 'middle';
    f.textAlign = 'center';

    drawSymbol(f, json["board"]["topLeft"], 0.5 * size, 0.55 * size)
    drawSymbol(f, json["board"]["topMid"], 1.5 * size, 0.55 * size)
    drawSymbol(f, json["board"]["topRight"], 2.5 * size, 0.55 * size)

    drawSymbol(f, json["board"]["midLeft"], 0.5 * size, 1.55 * size)
    drawSymbol(f, json["board"]["mid"], 1.5 * size, 1.55 * size)
    drawSymbol(f, json["board"]["midRight"], 2.5 * size , 1.55 * size)

    drawSymbol(f, json["board"]["bottomLeft"], 0.5 * size, 2.55 * size)
    drawSymbol(f, json["board"]["bottomMid"], 1.5 * size, 2.55 * size)
    drawSymbol(f, json["board"]["bottomRight"], 2.5 * size, 2.55 * size)

}

function drawSymbol(context, symbol, x, y) {
    if (symbol == "X") {
        context.fillStyle = getColour(1, gameJson)
    } else {
        context.fillStyle = getColour(2, gameJson)
    }
    context.fillText(symbol, x, y)
    context.fillStyle = "#FFFFFF"
    context.strokeText(symbol, x, y)
}

function sendMove(x, y, boxSize) {
    var key = ""
    if (y < boxSize) {
        key += "top"
    } else if (y < (2 * boxSize)) {
        key += "mid"
    } else {
        key += "bottom"
    }

    if (x < boxSize) {
        key += "Left"
    } else if (x < (2 * boxSize)) {
        key += "Mid"
    } else {
        key += "Right"
    }

    if (key == "midMid") {
        key = "mid"
    }

    var symbol = "X"
    if (gameJson["players"]["1"]["secretId"] == null) {
        symbol = "O"
    }

    if (gameJson["board"][key] == "") {
        socket.send(JSON.stringify(
            {
                board : {
                    [key] : symbol
                }
            }
        ))
    }
}
