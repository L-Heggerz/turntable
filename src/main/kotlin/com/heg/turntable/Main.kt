package com.heg.turntable

import com.fasterxml.jackson.databind.ObjectMapper
import com.heg.turntable.datasource.FakeDataStore
import com.heg.turntable.datasource.MergingJsonStore
import com.heg.turntable.datasource.Redis
import com.heg.turntable.games.GameRegistry
import com.heg.turntable.games.MoveExecutor
import com.heg.turntable.sockets.SocketManager
import com.heg.turntable.webserver.startWebServer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig
import java.lang.management.ManagementFactory
import java.net.URI


val log: Logger = LoggerFactory.getLogger("com.heg.turntable.Main")

fun main(args: Array<String>) {

    log.debug("JVM Arguments: ${ManagementFactory.getRuntimeMXBean().inputArguments}")
    log.debug("Program Arguments: ${args.map { it }}")

    log.info("Starting Application...")

    val expiryTime =
        if (System.getenv("TT_EXPIRY_TIME").isNullOrEmpty() ) {
            7 * 24 * 60 * 60
        } else {
            System.getenv("TT_EXPIRY_TIME").toLong()
        }

    val roomIdLength =
        if (System.getenv("TT_ROOM_ID_LENGTH").isNullOrEmpty() ) {
            4
        } else {
            System.getenv("TT_EXPIRY_TIME").toInt()
        }

    val jsonStore =
        if (System.getenv("TT_ENV") == "local")  {
                log.info("Running with an in memory data store")
                MergingJsonStore(
                    datasource = FakeDataStore(),
                    roomIdLength = roomIdLength
                )
        } else {
            buildRedisJsonStore(
                System.getenv("REDIS_URL"),
                expiryTime
            )
        }

    log.info("Connected to Datastore")

    val gameRegistry = GameRegistry(
        mapper = ObjectMapper()
    )

    log.info("Built Game Registry")

    val socketManager = SocketManager(
        moveExecutor = MoveExecutor(
            jsonStore,
            gameRegistry
        )
    )

    log.info("Initialised Socket Manager")

    startWebServer(
        port =
            if (System.getenv("PORT").isNullOrEmpty()) {
                8000
            } else {
                Integer.parseInt(System.getenv("PORT"))
            },
        socketManager = socketManager,
        gameRegistry = gameRegistry,
        jsonStore = jsonStore,
        roomIdLength = roomIdLength
    )

    log.info("Web Server Started")

}

private fun buildRedisJsonStore(url: String, expiryTime: Long): MergingJsonStore {
    log.info("Connecting to Redis at $url")
    val poolConfig = JedisPoolConfig()
    poolConfig.maxTotal = 10
    poolConfig.maxIdle = 5
    poolConfig.minIdle = 1
    poolConfig.testOnBorrow = true
    poolConfig.testOnReturn = true
    poolConfig.testWhileIdle = true
    return MergingJsonStore(
        datasource = Redis(
            JedisPool(
                poolConfig,
                URI.create(url)
            ),
            expiryTime
        )
    )
}
