package com.heg.turntable.webserver

import com.fasterxml.jackson.core.JsonParseException
import com.heg.turntable.datasource.MergingJsonStore
import com.heg.turntable.games.GameRegistry
import com.heg.turntable.sockets.SocketManager
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.InternalServerError
import io.ktor.http.cio.websocket.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.websocket.*
import java.io.File
import java.time.Duration

val htmlDir = File("src/main/html")
val getExemptions = listOf("gamelist", "datastore")

fun startWebServer(
    port: Int,
    socketManager: SocketManager,
    gameRegistry: GameRegistry,
    jsonStore: MergingJsonStore,
    roomIdLength: Int
) {
    embeddedServer(Netty, port) {
        install(StatusPages)
        install(CallLogging)
        install(WebSockets) {
            pingPeriod = Duration.ofSeconds(60) // Disabled (null) by default
            timeout = Duration.ofSeconds(15)
            maxFrameSize =
                Long.MAX_VALUE // Disabled (max value). The connection will be closed if surpassed this length.
            masking = false
        }
        routing {

            get("") {
                log.debug("Serving homepage")
                call.respondFile(htmlDir, "index.html")
            }

            get("{path...}") {
                val reqPath =
                    call
                        .parameters
                        .getAll("path")
                        .orEmpty()
                        .toMutableList()
                if (!getExemptions.contains(reqPath.first())) {
                    if (
                        reqPath.size == 2 &&
                        reqPath.first() in gameList() &&
                        reqPath.last().matches(Regex("[A-Z]{${roomIdLength}}"))
                    ) {
                        reqPath.removeLast()
                    }
                    val filePath: String = defaultToIndex(reqPath.joinToString("/"))
                    call.respondFile(htmlDir, filePath)
                }
            }

            get("/gamelist") {
                log.info("Serving gamelist")
                call.respond(
                    gameList()
                        .map { "\"$it\"" }
                        .toString()
                )
            }

            get("/datastore/{roomId}") {
                try {
                    val roomId = call.parameters["roomId"].orEmpty()
                    log.info("Serving json: ${jsonStore.read(roomId)}")
                    call.respond(jsonStore.read(roomId).toString())
                } catch (e: Exception) {
                    log.error(e.stackTraceToString())
                    call.respond(InternalServerError, "Internal Server Error")
                }
            }

            post("{path...}") {
                try {
                    val reqPath = call.parameters.getAll("path").orEmpty()
                    if (reqPath.size == 1) {
                        val body = call.receiveText()
                        log.info("Creating new room for ${reqPath.first()}")
                        log.debug("Post body: $body")
                        val roomId = jsonStore.newRoom(
                            gameRegistry.initialiseJson(body, reqPath.first())
                        )
                        call.respondRedirect("/${reqPath.first()}/$roomId")
                    } else {
                        log.warn("Room creation failed: invalid game name")
                        call.respond(BadRequest, "Invalid game name")
                    }
                } catch (e: JsonParseException) {
                    call.respond(BadRequest, "Invalid Json")
                } catch (e: Exception) {
                    log.error(e.stackTraceToString())
                    call.respond(InternalServerError, "Internal Server Error")
                }
            }

            webSocket("/chat/{room}/{userId}") {
                socketManager.registerChatSocket(
                    this,
                    call.parameters["room"].orEmpty(),
                    call.parameters["userId"].orEmpty()
                )
            }

            webSocket("/{game}/{room}/{userId}") {
                socketManager.registerGameSocket(
                    this,
                    call.parameters["room"].orEmpty(),
                    call.parameters["game"].orEmpty(),
                    call.parameters["userId"].orEmpty()
                )
            }

        }
    }.start(wait = true)
}

private fun defaultToIndex(reqPath: String) = if (reqPath.contains(".")) {
    reqPath
} else {
    "${reqPath}/index.html"
}

private fun gameList() =
    htmlDir
        .listFiles()!!
        .map { it.name }
        .filterNot { it.contains(".") }
        .filterNot { it == "template" }
        .filterNot { it == "js" }
        .filterNot { it == "chess"}

