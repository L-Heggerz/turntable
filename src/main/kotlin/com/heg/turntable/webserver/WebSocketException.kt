package com.heg.turntable.webserver

import java.lang.Exception

class WebSocketException(
    val errorMessage: String = "Unspecified Error"
) : Exception() {
    val json = """
        {
            "errorMessage": "$errorMessage"
        }
    """.trimIndent()

    override fun toString(): String = "WebException = $json"
}