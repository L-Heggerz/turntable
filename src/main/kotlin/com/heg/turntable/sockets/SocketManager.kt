package com.heg.turntable.sockets

import com.heg.turntable.games.MoveExecutor
import com.heg.turntable.webserver.WebSocketException
import io.ktor.http.cio.websocket.*
import io.ktor.websocket.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

class SocketManager(
    private val gameSocketMap: MutableMap<String, MutableMap<String, DefaultWebSocketServerSession>> = HashMap(),
    private val chatSocketMap: MutableMap<String, MutableMap<String, DefaultWebSocketServerSession>> = HashMap(),
    private val moveExecutor: MoveExecutor
) {
    private val log = LoggerFactory.getLogger(this.javaClass)

    suspend fun registerGameSocket(socket: DefaultWebSocketServerSession, roomId: String, gameName: String, userId: String) {
        try {
            if (roomId == "") {
                throw WebSocketException("You must have a room ID")
            }
            if (gameName == "") {
                throw WebSocketException("You must have a select a game")
            }
            if (userId == "") {
                throw WebSocketException("You must have a user ID")
            }
            log.info("Registering new game socket to room $roomId")
            if (!gameSocketMap.containsKey(roomId)) {
                gameSocketMap[roomId] = mutableMapOf(userId to socket)
            } else {
                gameSocketMap[roomId]!![userId] = socket
            }
            for (frame in socket.incoming) {
                try {
                    when (frame) {
                        is Frame.Text -> {
                            coroutineScope {
                                launch {
                                    try {
                                        val data = frame.readText()
                                        log.info("Recieved $data from $userId")
                                        moveExecutor.write(data, userId, roomId, gameName)
                                        updateGameSockets(
                                                roomId,
                                                gameName
                                        )
                                    } catch (e: WebSocketException) {
                                        socket.send(e.json)
                                    }
                                }
                            }
                        }
                        is Frame.Binary -> {
                            log.warn("Unexpected Binary Frame type. Closing Socket.")
                            socket.close()
                            gameSocketMap[roomId]!!.remove(userId)
                        }
                        is Frame.Close -> {
                            log.info("Socket closed.")
                            socket.close()
                            gameSocketMap[roomId]!!.remove(userId)
                        }
                        is Frame.Ping -> socket.send(Frame.Pong(frame.readBytes()))
                        is Frame.Pong -> {
                            log.warn("Unexpected Pong Frame type. Closing Socket.")
                            socket.close()
                            gameSocketMap[roomId]!!.remove(userId)
                        }
                    }
                } catch (e: WebSocketException) {
                    log.error("WebSocketException Caught - ${e.errorMessage}")
                    socket.send(e.json)
                }
            }
        } catch (e: WebSocketException) {
            log.error("WebSocketException Caught - ${e.errorMessage}")
            socket.send(e.json)
        }
    }

    suspend private fun updateGameSockets(roomId: String, gameName: String) =
        if (gameSocketMap.containsKey(roomId)) {
            gameSocketMap[roomId]!!.forEach {
                val gameJson = moveExecutor.read(roomId, gameName, it.key)
                coroutineScope {
                    launch {
                        it.value.send(Frame.Text(gameJson.toString()))
                    }
                }
            }
        }
        else {
            throw WebSocketException("No Connections found for this room!")
        }

    suspend fun registerChatSocket(
        socket: DefaultWebSocketServerSession,
        roomId: String,
        userId: String
    ) {
        if (!chatSocketMap.containsKey(roomId)) {
            chatSocketMap[roomId] = mutableMapOf(userId to socket)
        } else {
            chatSocketMap[roomId]!![userId] = socket
        }
        log.info("Registering new chat socket for room $roomId")
        for (frame in socket.incoming) {
            when (frame) {
                is Frame.Text -> {
                    val data = frame.readText()
                    if (chatSocketMap.containsKey(roomId)) {
                        chatSocketMap[roomId]!!.forEach {
                            coroutineScope {
                                launch {
                                    it.value.send(Frame.Text(data))
                                }
                            }
                        }
                    }
                }
                is Frame.Binary -> {
                    log.warn("Unexpected Binary Frame type. Closing Socket.")
                    socket.close()
                }
                is Frame.Close -> {
                    log.info("Socket closed.")
                    socket.close()
                }
                is Frame.Ping -> socket.send(Frame.Pong(frame.readBytes()))
                is Frame.Pong -> {
                    log.warn("Unexpected Pong Frame type. Closing Socket.")
                    socket.close()
                }
            }
        }
    }
}