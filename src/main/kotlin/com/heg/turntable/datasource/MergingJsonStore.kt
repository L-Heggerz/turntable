package com.heg.turntable.datasource

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.slf4j.LoggerFactory
import java.util.concurrent.ThreadLocalRandom
import kotlin.streams.asSequence

class MergingJsonStore (
    private val datasource: Datasource,
    private val parser: ObjectMapper = ObjectMapper(),
    private val roomIdCharacters: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    private val roomIdLength: Int = 4
){
    private val uneditableFields = listOf("gameName")
    private val log = LoggerFactory.getLogger(this.javaClass)

    fun write(roomId: String, moveJson: JsonNode) {
        datasource.write(roomId, mergeJson(parser.readTree(datasource.read(roomId)).deepCopy(), moveJson).toString())
    }

    fun read(roomId: String): JsonNode = parser.readTree(datasource.read(roomId))

    private fun mergeJson(oldJson: ObjectNode, newJson: JsonNode): JsonNode {
        log.debug("Old JSON: $oldJson")
        log.debug("New JSON: $newJson")
        for ( field: String in newJson.fieldNames() ) {
            val newNode = newJson[field]
            if ( oldJson.has(field) ) {
                if ( field in uneditableFields )
                    continue
                if ( !oldJson[field].isNull && newNode.isObject ) {
                    oldJson.put(field, mergeJson(oldJson[field].deepCopy(), newNode))
                }
                else {
                    oldJson.put(field, newNode)
                }
            }
            else {
                oldJson.put(field, newNode)
            }
        }
        log.debug("Merged JSON: $oldJson")
        return oldJson
    }

    fun newRoom(initialiseJson: JsonNode): String {
        var roomId: String

        do {
            roomId = ThreadLocalRandom.current()
                .ints(roomIdLength.toLong(), 0, roomIdCharacters.length)
                .asSequence()
                .map(roomIdCharacters::get)
                .joinToString("")
        } while ( datasource.keyExists(roomId) )

        //TODO: Max number of attempts?
        log.info("Creating room $roomId")

        datasource.write(roomId, initialiseJson.toString())

        return roomId
    }
}