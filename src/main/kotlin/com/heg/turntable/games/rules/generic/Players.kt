package com.heg.turntable.games.rules.generic

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.webserver.WebSocketException

// TODO: Ready up
// TODO: Minimum Players vs Maximum Players

class Players(
    private val playerCount: Int = 1,
    private val mapper: ObjectMapper
) : Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {
        val players = mapper.createObjectNode()
        for (i in 1..playerCount) {
            players.putNull(i.toString())
        }
        moveJson.put("players", players)
        return moveJson
    }

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        val playerCount = originalJson["players"]
            .fields()
            .asSequence()
            .filter { !it.value.isNull }
            .toList()
            .size
        when {
            playerCount < this.playerCount -> {
                if (
                    moveJson
                        .fieldNames()
                        .asSequence()
                        .filter { !listOf("players").contains(it) }
                        .toList()
                        .isNotEmpty()
                ) {
                    throw WebSocketException("Not enough players in the game")
                }
                else {
                    if (moveJson.fieldNames().asSequence().contains("players")) {
                        for (pId: String in moveJson["players"].fieldNames()) {
                            if (!originalJson.get("players").get(pId).isNull) {
                                throw WebSocketException("You cannot overwrite an existing player")
                            }
                        }
                    }
                    return moveJson
                }
            }
            playerCount > playerCount -> {
                throw WebSocketException("Game is overloaded!")
            }
            else -> {
                if (moveJson.fieldNames().asSequence().contains("players")) {
                    throw WebSocketException("Game is full!")
                }
                else {
                    if (
                        originalJson["players"]
                            .fields()
                            .asSequence()
                            .map{ it.value["secretId"].textValue() }
                            .toList()
                            .contains(playerId)
                    ) {
                        return moveJson
                    } else {
                        throw WebSocketException("You are not a player in this game!")
                    }
                }
            }
        }
    }

    override fun alterRead(originalJson: ObjectNode, playerId: String): JsonNode {
        val cleanNode = mapper.createObjectNode()

        originalJson.get("players").fieldNames().forEach {
            val playerNode = mapper.createObjectNode()
            playerNode.put("name", originalJson.get("players").get(it).get("name"))
            if (
                !originalJson.get("players").get(it).isNull &&
                playerId == originalJson.get("players").get(it).get("secretId").textValue()
            ) {
                playerNode.put("secretId", originalJson.get("players").get(it).get("secretId"))
            }
            cleanNode.put(it, playerNode)
        }

        originalJson.put(
            "players",
            cleanNode
        )

        return originalJson
    }
}