package com.heg.turntable.games.rules.specific.chess

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.games.rules.specific.chess.pieces.*

class Chess(
        private val mapper: ObjectMapper
) : Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {
        val board = mapper.createObjectNode()

        "ABCDEFGH".forEach{
            val columnNode = mapper.createObjectNode()
            for (i in 1 .. 8) {
                initialisePiece(it, i, columnNode)
            }
            board.put(it.toString(), columnNode)
        }

        moveJson.put("board", board)
        return moveJson
    }

    private fun initialisePiece(letter: Char, number: Int, columnNode: ObjectNode) {
        if(number in 3 .. 6) {
            return
        }

        val isBlack = ((number == 7) || (number == 8))

        //create pawns
        val piece = if ((number == 2) || (number == 7)) {
            Pawn(mapper, isBlack, letter, number)
        }

        //create other pieces
        else {
            when (letter) {
                'A', 'H' -> Rook(mapper, isBlack, letter, number)
                'B', 'G' -> Knight(mapper, isBlack, letter, number)
                'C', 'F' -> Bishop(mapper, isBlack, letter, number)
                'D' -> Queen(mapper, isBlack, letter, number)
                'E' -> King(mapper, isBlack, letter, number)
                else -> throw Exception("Invalid column, off board!")
            }
        }

        columnNode.put(number.toString(), piece.toJson())
    }



}