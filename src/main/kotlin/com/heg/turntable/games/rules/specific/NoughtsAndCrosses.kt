package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.webserver.WebSocketException

class NoughtsAndCrosses(
    private val mapper: ObjectMapper
) : Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {
        val board = mapper.createObjectNode()
        board.put("topLeft", "")
        board.put("topMid", "")
        board.put("topRight", "")
        board.put("midLeft", "")
        board.put("mid", "")
        board.put("midRight", "")
        board.put("bottomLeft", "")
        board.put("bottomMid", "")
        board.put("bottomRight", "")

        moveJson.put("board", board)

        return moveJson
    }

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        if ( moveJson.has("board") ) {
            val alteredValues = moveJson["board"].fields().asSequence().map { it.value.textValue() }.toList()

            if (alteredValues.size > 1) {
                throw WebSocketException("You can only change one field at a time")
            }

            if (alteredValues.filterNot { it == "X" || it == "O" }.isNotEmpty()) {
                throw WebSocketException("Illegal character on board")
            }
        }

        return moveJson
    }

}