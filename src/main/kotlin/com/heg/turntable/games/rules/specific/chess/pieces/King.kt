package com.heg.turntable.games.rules.specific.chess.pieces

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode

class King(
    private val mapper: ObjectMapper,
    private val isBlack: Boolean,
    private val x: Char,
    private val y: Int = if (isBlack) 8 else 1,
    private val colour: String = if (isBlack) "black" else "white",
    private val hasMoved: Boolean = false
): Piece {
    private val name: String = "king"

    override fun isValidMove(moveJson: ObjectNode): Boolean {
        TODO("Not yet implemented")

        return false
    }

    override fun toJson(): ObjectNode =
        mapper.readTree("""
            {
                "name": "$name",
                "colour": "$colour",
                "hasMoved": "$hasMoved"
            }
        """).deepCopy()

}
