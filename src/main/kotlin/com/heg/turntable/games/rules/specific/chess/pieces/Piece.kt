package com.heg.turntable.games.rules.specific.chess.pieces

import com.fasterxml.jackson.databind.node.ObjectNode

interface Piece {
    fun isValidMove(moveJson: ObjectNode): Boolean
    fun toJson(): ObjectNode
}