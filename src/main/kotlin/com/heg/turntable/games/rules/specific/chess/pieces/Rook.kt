package com.heg.turntable.games.rules.specific.chess.pieces

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode

class Rook(
    private val mapper: ObjectMapper,
    private val isBlack: Boolean,
    private val x: Char,
    private val y: Int = if (isBlack) 8 else 1,
    private val hasMoved: Boolean = false
): Piece {
    private val name: String = "rook"
    private val colour: String = if (isBlack) "black" else "white"

    override fun isValidMove(moveJson: ObjectNode): Boolean {
        TODO("Not yet implemented")

        return false
    }

    override fun toJson(): ObjectNode =
            mapper.readTree("""
            {
                "name": "$name",
                "colour": "$colour",
                "hasMoved": "$hasMoved"
            }
        """).deepCopy()
}