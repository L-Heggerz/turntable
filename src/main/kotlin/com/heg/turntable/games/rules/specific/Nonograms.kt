package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.games.rules.generic.emptyGrid
import kotlin.random.Random

class Nonograms(
    val mapper: ObjectMapper
) : Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode =
        newNonogram(
            moveJson,
            moveJson["width"].textValue().toInt(),
            moveJson["height"].textValue().toInt()
        )

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        if ( moveJson.has("newNonogram") && moveJson.get("newNonogram").booleanValue() ) {
            moveJson.remove("newNonogram")
            if ( moveJson.has("width") && moveJson.has("height") ) {
                newNonogram(
                    moveJson,
                    moveJson["width"].textValue().toInt(),
                    moveJson["height"].textValue().toInt()
                )
            }
            else {
                newNonogram(
                    moveJson,
                    originalJson["width"].textValue().toInt(),
                    originalJson["height"].textValue().toInt()
                )
            }
        }
        return moveJson
    }

    private fun newNonogram(
        moveJson: ObjectNode,
        width: Int,
        height: Int
    ): ObjectNode {
        moveJson.put("grid", emptyGrid(width, height))

        val image = NonogramImage(height, width, mapper)

        moveJson.put("topClues", image.generateTopNumbers())
        moveJson.put("leftClues", image.generateLeftNumbers())
        return moveJson
    }

}

fun generateNonogramImage(height: Int, width: Int): List<List<Boolean>> {
    val image : MutableList<List<Boolean>> = mutableListOf()
    for ( x in 0 until width ) {
        val row: MutableList<Boolean> = mutableListOf()
        for ( y in 0 until height ) {
            row.add(Random.nextBoolean())
        }
        image.add(row)
    }
    return image
}

class NonogramImage (
    private val width: Int,
    private val height: Int,
    private val mapper: ObjectMapper,
    private val nonogramImage: List<List<Boolean>> = generateNonogramImage(width, height)
) {

    fun generateTopNumbers(): ObjectNode {
        val topClues = mapper.createObjectNode()
        for ((k, row) in nonogramImage.withIndex()) {
            val numbers: ArrayNode = mapper.createArrayNode()
            var count = 0
            for ( square in row ) {
                if ( square ) {
                    count++
                } else if (count > 0) {
                    numbers.add(count)
                    count = 0
                }
            }
            if (count > 0) {
                numbers.add(count)
            }
            if ( numbers.size() == 0 ) {
                numbers.add(0)
            }
            topClues.put(k.toString(), numbers)
        }
        return topClues
    }

    fun generateLeftNumbers(): ObjectNode {
        val topClues = mapper.createObjectNode()
        for ( i in 0 until nonogramImage[0].size ) {
            val numbers: ArrayNode = mapper.createArrayNode()
            var count = 0
            for (row in nonogramImage) {
                if ( row[i] ) {
                    count++
                } else if (count > 0) {
                    numbers.add(count)
                    count = 0
                }
            }
            if (count > 0) {
                numbers.add(count)
            }
            if ( numbers.size() == 0 ) {
                numbers.add(0)
            }
            topClues.put(i.toString(), numbers)
        }

        return topClues
    }
}