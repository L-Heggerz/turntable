package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule

class Foursies(
    private val mapper: ObjectMapper
) : Rule {
    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {
        val board = mapper.createArrayNode()
        board.add(createRow(1))
        board.add(createRow(0))
        board.add(createRow(0))
        board.add(createRow(2))
        moveJson.put("board", board)
        return moveJson
    }

    private fun createRow(num: Int): ArrayNode {
        val row = mapper.createArrayNode()
        row.add(num)
        row.add(num)
        row.add(num)
        row.add(num)
        return row
    }

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        return moveJson
    }
}