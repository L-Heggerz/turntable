package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.webserver.WebSocketException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail

internal class BattleShipsTest {

    val mapper = ObjectMapper()

    private fun isEmptyGridOfSize(json: JsonNode, size: Int) {
        for (i in 1..size) {
            for (j in 1..size) {
                assertTrue(json[i.toString()].has(j.toString()))
            }
            assertFalse(json[i.toString()].has((size + 1).toString()))
        }
        assertFalse(json.has((size + 1).toString()))
    }

    @Test
    fun initializes_game_with_four_grids_of_correct_size() {
        val output = BattleShips(mapper).initialiseMap(mapper.readTree("""{ "size": "5" }""").deepCopy())
        isEmptyGridOfSize(output["boats"]["1"], 5)
        isEmptyGridOfSize(output["boats"]["2"], 5)
        isEmptyGridOfSize(output["shots"]["1"], 5)
        isEmptyGridOfSize(output["shots"]["2"], 5)
    }

    @Test
    fun opponents_boats_are_hidden_when_reading() {
        val state: ObjectNode = mapper.readTree("""
            {
                "players" : {
                    "1" : {
                        "secretId" : "id1",
                        "name" : "whatever"
                    },
                    "2" : {
                        "secretId" : "id2",
                        "name" : "whatever"
                    }
                },
                "boats" : {
                    "1" : "boats",
                    "2" : "boats"
                }
            }
        """).deepCopy()
        val rule = BattleShips(mapper)

        assertTrue(rule.alterRead(state, "id1")["boats"].has("1"))
        assertFalse(rule.alterRead(state, "id1")["boats"].has("2"))

        assertTrue(rule.alterRead(state, "id2")["boats"].has("2"))
        assertFalse(rule.alterRead(state, "id2")["boats"].has("1"))
    }

    @Test
    fun shots_are_marked_as_a_miss_if_no_boat_present() {
        assertEquals(
            "MISS",
            BattleShips(mapper).alterWrite(
                mapper.readTree("""
                {
                    "boats" : {
                        "1" : { "1" : { "1" : "" } },
                        "2" : { "1" : { "1" : "" } }
                    },
                    "shots" : {
                        "1" : { "1" : { "1" : "" } },
                        "2" : { "1" : { "1" : "" } }
                    }
                }
            """).deepCopy(),
                mapper.readTree("""{ "shots" : { "1" : { "1" : { "1" : "X" } } } } }""").deepCopy(),
                "blah"
            )["shots"]["1"]["1"]["1"].textValue()
        )
    }

    @Test
    fun shots_are_marked_as_a_hit_if_a_boat_is_present() {
        assertEquals(
            "HIT",
            BattleShips(mapper).alterWrite(
                mapper.readTree("""
                {
                    "boats" : {
                        "1" : { "1" : { "1" : "" } },
                        "2" : { "1" : { "1" : "X" } }
                    },
                    "shots" : {
                        "1" : { "1" : { "1" : "" } },
                        "2" : { "1" : { "1" : "" } }
                    }
                }
            """).deepCopy(),
                mapper.readTree("""{ "shots" : { "1" : { "1" : { "1" : "X" } } } } }""").deepCopy(),
                "blah"
            )["shots"]["1"]["1"]["1"].textValue()
        )
    }

    @Test
    fun placing_overlapping_ships_causes_exception() {
        try {
            BattleShips(mapper).alterWrite(
                mapper.readTree("""
                {
                    "readys" : { "1" : false, "2" : false },
                    "players" : { "1" : { "secretId" : "blah" } },
                    "boats" : {
                        "1" : {
                            "1" : {
                                "1" : "^",
                                "2" : "v"
                            },
                            "2" : {
                                "1" : "",
                                "2" : ""
                            }
                        }
                    }
                }
                """),
                mapper.readTree("""
                {
                    "boats" : {
                        "1" : {
                            "1" : {
                                "1" : "<"
                            },
                            "2" : {
                                "1": ">"
                            }
                        }
                    }
                }
                """).deepCopy(),
                "blah"
            )
            fail("No exception thrown")
        } catch (e: WebSocketException) {
            assertEquals("Ships cannot overlap", e.errorMessage)
        }
    }

}