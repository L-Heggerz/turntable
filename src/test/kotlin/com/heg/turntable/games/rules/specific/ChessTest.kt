package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.ObjectMapper
import com.heg.turntable.games.rules.specific.chess.Chess
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class ChessTest {

    private val mapper = ObjectMapper()

    @Test
    fun Initialize_map_fills_correct_spaces() {
        val chess = Chess(mapper)
        val board = chess.initialiseMap(mapper.createObjectNode())["board"]
        board.fields().asSequence().forEach {
            assertNotEquals("{}", it.value["1"].toString())
            assertNotEquals("{}", it.value["2"].toString())
            assertNull(it.value["3"])
            assertNull(it.value["4"])
            assertNull(it.value["5"])
            assertNull(it.value["6"])
            assertNotEquals("{}", it.value["7"].toString())
            assertNotEquals("{}", it.value["8"].toString())
        }
    }

    @Test
    fun Initialise_map_sets_kings_correctly() {
        val chess = Chess(mapper)
        val board = chess.initialiseMap(mapper.createObjectNode())["board"]
        assertEquals(mapper.readTree(BLACK_KING_INIT_JSON), board["E"]["8"])
        assertEquals(mapper.readTree(WHITE_KING_INIT_JSON), board["E"]["1"])
    }

    @Test
    fun Initialise_map_sets_queens_correctly() {
        val chess = Chess(mapper)
        val board = chess.initialiseMap(mapper.createObjectNode())["board"]
        assertEquals(mapper.readTree(BLACK_QUEEN_JSON), board["D"]["8"])
        assertEquals(mapper.readTree(WHITE_QUEEN_JSON), board["D"]["1"])
    }

    @Test
    fun Initialise_map_sets_bishops_correctly() {
        val chess = Chess(mapper)
        val board = chess.initialiseMap(mapper.createObjectNode())["board"]
        assertEquals(mapper.readTree(BLACK_BISHOP_JSON), board["C"]["8"])
        assertEquals(mapper.readTree(BLACK_BISHOP_JSON), board["F"]["8"])
        assertEquals(mapper.readTree(WHITE_BISHOP_JSON), board["C"]["1"])
        assertEquals(mapper.readTree(WHITE_BISHOP_JSON), board["F"]["1"])
    }

    @Test
    fun Initialise_map_sets_knights_correctly() {
        val chess = Chess(mapper)
        val board = chess.initialiseMap(mapper.createObjectNode())["board"]
        assertEquals(mapper.readTree(BLACK_KNIGHT_JSON), board["B"]["8"])
        assertEquals(mapper.readTree(BLACK_KNIGHT_JSON), board["G"]["8"])
        assertEquals(mapper.readTree(WHITE_KNIGHT_JSON), board["B"]["1"])
        assertEquals(mapper.readTree(WHITE_KNIGHT_JSON), board["G"]["1"])
    }

    @Test
    fun Initialise_map_sets_rooks_correctly() {
        val chess = Chess(mapper)
        val board = chess.initialiseMap(mapper.createObjectNode())["board"]
        assertEquals(mapper.readTree(BLACK_ROOK_INIT_JSON), board["A"]["8"])
        assertEquals(mapper.readTree(BLACK_ROOK_INIT_JSON), board["H"]["8"])
        assertEquals(mapper.readTree(WHITE_ROOK_INIT_JSON), board["A"]["1"])
        assertEquals(mapper.readTree(WHITE_ROOK_INIT_JSON), board["H"]["1"])
    }

    @Test
    fun Initialise_map_sets_pawns_correctly() {
        val chess = Chess(mapper)
        val board = chess.initialiseMap(mapper.createObjectNode())["board"]
        board.fields().asSequence().forEach {
            assertEquals(mapper.readTree(BLACK_PAWN_INIT_JSON), it.value["7"])
            assertEquals(mapper.readTree(WHITE_PAWN_INIT_JSON), it.value["2"])
        }
    }

    private val BLACK_KING_INIT_JSON = """
        {
                "name": "king",
                "colour": "black",
                "hasMoved": "false"
            }
    """

    private val WHITE_KING_INIT_JSON = """
        {
                "name": "king",
                "colour": "white",
                "hasMoved": "false"
            }
    """

    private val BLACK_QUEEN_JSON = """
        {
                "name": "queen",
                "colour": "black"
            }
    """

    private val WHITE_QUEEN_JSON = """
        {
                "name": "queen",
                "colour": "white"
            }
    """

    private val BLACK_BISHOP_JSON = """
        {
                "name": "bishop",
                "colour": "black"
            }
    """

    private val WHITE_BISHOP_JSON = """
        {
                "name": "bishop",
                "colour": "white"
            }
    """

    private val BLACK_KNIGHT_JSON = """
        {
                "name": "knight",
                "colour": "black"
            }
    """

    private val WHITE_KNIGHT_JSON = """
        {
                "name": "knight",
                "colour": "white"
            }
    """

    private val BLACK_ROOK_INIT_JSON = """
        {
                "name": "rook",
                "colour": "black",
                "hasMoved": "false"
            }
    """

    private val WHITE_ROOK_INIT_JSON = """
        {
                "name": "rook",
                "colour": "white",
                "hasMoved": "false"
            }
    """

    private val BLACK_PAWN_INIT_JSON = """
        {
                "name": "pawn",
                "colour": "black",
                "hasMoved": "false"
            }
    """

    private val WHITE_PAWN_INIT_JSON = """
        {
                "name": "pawn",
                "colour": "white",
                "hasMoved": "false"
            }
    """
}