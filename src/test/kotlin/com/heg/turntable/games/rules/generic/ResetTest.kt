package com.heg.turntable.games.rules.generic

import com.fasterxml.jackson.databind.ObjectMapper
import com.heg.turntable.webserver.WebSocketException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail

internal class ResetTest {

    val mapper = ObjectMapper()

    @Test
    fun you_can_add_your_own_vote_to_reset_to_the_list() {
        val rule = Reset(
            "reset",
            "true",
            mapper
        )

        assertEquals(
            mapper.readTree("""
                    {
                        "resetVotes" : ["me"] 
                    }"""
            ).deepCopy(),
            rule.alterWrite(
                originalJson = mapper.readTree("""
                    {
                        "players" : {
                            "1" : {
                                "name" : "me",
                                "secretId" : "secret"
                            },
                            "2" : {
                                "name" : "notMe",
                                "secretId" : "secret2"
                            }
                        },
                        "resetVotes" : [] 
                    }"""
                ),
                moveJson = mapper.readTree("""
                    {
                        "resetVotes" : ["me"] 
                    }"""
                ).deepCopy(),
                "secret"
            )
        )
    }

    @Test
    fun you_cannot_add_someone_elses_vote_to_reset_to_the_list() {
        val rule = Reset(
            "reset",
            "true",
            mapper
        )

        try {
            rule.alterWrite(
                originalJson = mapper.readTree(
                    """
                    {
                        "players" : {
                            "1" : {
                                "name" : "me",
                                "secretId" : "secret"
                            },
                            "2" : {
                                "name" : "notMe",
                                "secretId" : "secret2"
                            }
                        },
                        "resetVotes" : [] 
                    }"""
                ),
                moveJson = mapper.readTree(
                    """
                    {
                        "resetVotes" : ["notMe"] 
                    }"""
                ).deepCopy(),
                "secret"
            )
            fail("No Exception Thrown")
        } catch (e: WebSocketException) {
            assertEquals("You cannot vote reset for another player", e.errorMessage)
        }
    }

    @Test
    fun when_there_is_already_a_vote_to_reset_your_vote_is_added_to_the_list() {
        val rule = Reset(
            "reset",
            "true",
            mapper
        )

        assertEquals(
            mapper.readTree("""
                    {
                        "resetVotes" : ["me", "notMe"] 
                    }"""
            ).deepCopy(),
            rule.alterWrite(
                originalJson = mapper.readTree("""
                    {
                        "players" : {
                            "1" : {
                                "name" : "me",
                                "secretId" : "secret"
                            },
                            "2" : {
                                "name" : "notMe",
                                "secretId" : "secret2"
                            },
                            "3" : {
                                "name" : "notMeToo",
                                "secretId" : "secret3"
                            }
                        },
                        "resetVotes" : ["notMe"] 
                    }"""
                ),
                moveJson = mapper.readTree("""
                    {
                        "resetVotes" : ["me"] 
                    }"""
                ).deepCopy(),
                "secret"
            )
        )
    }

    @Test
    fun when_all_votes_are_in_reset_value_is_written() {
        val rule = Reset(
            "reset",
            "true",
            mapper
        )

        assertEquals(
            mapper.readTree("""
                    {
                        "resetVotes" : [],
                        "reset" : true
                    }"""
            ).deepCopy(),
            rule.alterWrite(
                originalJson = mapper.readTree("""
                    {
                        "players" : {
                            "1" : {
                                "name" : "me",
                                "secretId" : "secret"
                            },
                            "2" : {
                                "name" : "notMe",
                                "secretId" : "secret2"
                            }
                        },
                        "reset" : "false",
                        "resetVotes" : ["notMe"] 
                    }"""
                ),
                moveJson = mapper.readTree("""
                    {
                        "resetVotes" : ["me"] 
                    }"""
                ).deepCopy(),
                "secret"
            )
        )
    }

    @Test
    fun duplicates_cannot_be_added_to_reset_votes() {
        val rule = Reset(
            "reset",
            "true",
            mapper
        )

        assertEquals(
            mapper.readTree("""
                    {
                        "resetVotes" : ["me"] 
                    }"""
            ).deepCopy(),
            rule.alterWrite(
                originalJson = mapper.readTree("""
                    {
                        "players" : {
                            "1" : {
                                "name" : "me",
                                "secretId" : "secret"
                            },
                            "2" : {
                                "name" : "notMe",
                                "secretId" : "secret2"
                            }
                        },
                        "resetVotes" : ["me"] 
                    }"""
                ),
                moveJson = mapper.readTree("""
                    {
                        "resetVotes" : ["me"] 
                    }"""
                ).deepCopy(),
                "secret"
            )
        )
    }
}