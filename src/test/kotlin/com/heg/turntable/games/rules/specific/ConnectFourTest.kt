package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.webserver.WebSocketException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail

class ConnectFourTest {

    private val mapper = ObjectMapper()
    private val initNode: ObjectNode = mapper.readTree("""{"width" : "7", "height" : "6"}""").deepCopy()

    @Test
    fun initializes_map_with_empty_arrays() {
        val rule = ConnectFour(mapper)
        for (column in rule.initialiseMap(initNode)["board"].fields()) {
            assertEquals(mapper.createArrayNode(), column.value)
        }
    }

    @Test
    fun column_values_are_put_in_arrays() {
        val rule = ConnectFour(mapper)
        assertEquals(
            mapper.readTree("""["A"]"""),
            rule.alterWrite(
                rule.initialiseMap(initNode),
                mapper.readTree(
                    """
                        {
                            "board" : {
                                "1" : "A"
                            }
                        }
                    """
                ).deepCopy(),
                "blah"
            )["board"]["1"]
        )
    }

    @Test
    fun column_values_are_added_to_the_end_arrays() {
        val rule = ConnectFour(mapper)
        assertEquals(
            mapper.readTree("""["A", "B"]"""),
            rule.alterWrite(
                mapper.readTree(
                    """
                        {
                            "width" : "7",
                            "height" : "6",
                            "board" : {
                                "1" : [ "A" ]
                            }
                        }
                    """
                ),
                mapper.readTree(
                    """
                        {
                            "board" : {
                                "1" : "B"
                            }
                        }
                    """
                ).deepCopy(),
                "blah"
            )["board"]["1"]
        )
    }

    @Test
    fun order_is_preserved_when_adding_items_to_arrays() {
        val rule = ConnectFour(mapper)
        assertEquals(
            mapper.readTree("""["A", "Z", "Potato"]"""),
            rule.alterWrite(
                mapper.readTree(
                    """
                        {
                            "width" : "7",
                            "height" : "6",
                            "board" : {
                                "2" : [ "A", "Z" ]
                            }
                        }
                    """
                ),
                mapper.readTree(
                    """
                        {
                            "board" : {
                                "2" : "Potato"
                            }
                        }
                    """
                ).deepCopy(),
                "blah"
            )["board"]["2"]
        )
    }

    @Test
    fun exception_thrown_when_column_at_size_specified_by_height_value() {
        val rule = ConnectFour(mapper)
        try {
            rule.alterWrite(
                mapper.readTree(
                    """
                        {
                            "width" : "7",
                            "height" : "6",
                            "board" : {
                                "4" : [ "1", "2", "3", "4", "5", "6" ]
                            }
                        }
                    """
                ),
                mapper.readTree(
                    """
                        {
                            "board" : {
                                "4" : "7"
                            }
                        }
                    """
                ).deepCopy(),
                "blah"
            )
            fail("No WebSocketException thrown when column size exceeded")
        } catch (e: WebSocketException) {
            assertEquals(
                "Column is full",
                e.errorMessage
            )
        }
        try {
            rule.alterWrite(
                mapper.readTree(
                    """
                            {
                                "width" : "7",
                                "height" : "5",
                                "board" : {
                                    "4" : [ "1", "2", "3", "4", "5" ]
                                }
                            }
                        """
                ),
                mapper.readTree(
                    """
                            {
                                "board" : {
                                    "4" : "6"
                                }
                            }
                        """
                ).deepCopy(),
                "blah"
            )
            fail("No WebSocketException thrown when column size exceeded")
        } catch (e: WebSocketException) {
            assertEquals(
                "Column is full",
                e.errorMessage
            )
        }
    }
}