FROM gradle:7-jdk11 AS build
COPY . .
RUN gradle shadowJar

FROM openjdk:11-jre-slim
RUN ls
RUN ls *
COPY --from=build /home/gradle/ /
EXPOSE 8080
ENV REDIS_URL redis://red-cdfg84kgqg4d3ghms5p0:6379
ENTRYPOINT ["java", "-jar", "/build/libs/turntable-all.jar", "env=heroku"]
