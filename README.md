
# TurnTable

Turntable is a service for creating multiplayer browser games. It serves web pages to display the games user interface,
recieves updates to the state of the game via WebSockets, transforms these updates according to a configured set
of rules and then saves them to a datastore (currently Redis).

Players are grouped together in lobbys where they all share access to the same entry in the datastore. They send updates
in JSON format to their WebSockets. These JSONs are transformed by a series of Rules defined in the backend and are 
merged with the existing JSON. Arrays are NOT concatenated, they must be overwritten. Once any change is made, the 
updated game state is sent to all connected players.

The backend is written in Kotlin and the front end is plain HTML and JavaScript. Although contributers are welcome
to use any front end technology they like for their games.


## Useful commands

### Build the code

Compiles the kotlin code and its dependencies into a fat jar:

```
make jar
```

### Test the code

```
make test
```

### Run the code

#### To run the code locally:

```
make run-local
```

You should be able to see your application running at http://localhost:8000/. No Redis is required as all game data
will be kept in memory. This will cause data to be lost upon restart and could cause OOMs if games grow outrageously
large.

#### To run the code on Heroku:

```
make run-heroku
```

This will use environment variables to configure the service.

#### To run the code on some other environment

```
java -jar build/libs/turntable-all.jar env=vm redisUrl={YOUR REDIS URL HERE} webServerPort={YOUR PORT HERE}
```

You can see an example in the Makefile under ```make run-vm```. This allows you to pass in your redis url and your port
number as command line arguments in the way templated above.


## Contributing

To add a new game to the TurnTable library, firstly you should run:

```
make new-game
```

This will create a html directory with 3 skeleton files: `thumb.html`, `form.html` and `index.html`. 

 * `thumb.html` is the small preview of the game that will appear on the homepage. This sould be updated with an 
    appropriate description and 250px x 250px image.
 * `form.html` is for any custom data that is needed to initialise the room before any players join.
 * `index.html` is the page for displaying the game itself. It should come ready with a WebSocket that you should use to
    communicate with the back end server.

You can update just these to make a multiplayer game that works between browsers and is persisted.

If you want to use any of the built-in rules e.g. "this is a 2-player game", or "players take turns",
you will need to add your game to src/main/kotlin/com/heg/turntable/games/GameRegistry.kt.

If you want to provide serverside security to stop any cheating, you will want to write your own
rules that validate moves. Further information on how to write rules can be found in the rules
interface.
